package controllers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"rest-api/src/config"
	"rest-api/src/models"
	"github.com/spf13/cast"
	"gorm.io/gorm"
)

// Define database client
var db *gorm.DB = config.ConnectDB()

// User struct for request body
type userRequest struct {
	Name string `json:"name"`
	Email string `json:"email"`
	Age int `json:"age"`
}

// Defining struct for response
type userResponse struct {
	userRequest
	ID uint `json:"id"`
}

// Getting all user datas
func GetAllUsers(context *gin.Context) {
	var users []models.User

  // Querying to find todo datas.
	err := db.Find(&users)
	if err.Error != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Error getting data"})
		return
	}

  // Creating http response
	context.JSON(http.StatusOK, gin.H{
		"status":  "200",
		"message": "Success",
		"data":    users,
	})
}

// Create user data to database by run this function
func CreateUser(context *gin.Context) {
	var data userRequest

  // Binding request body json to request body struct
	if err := context.ShouldBindJSON(&data); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

  // Matching user models struct with user request struct
	user := models.User{}
	user.Name = data.Name
	user.Email = data.Email
	user.Age = data.Age

  // Querying to database
	result := db.Create(&user)
	if result.Error != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Something went wrong"})
		return
	}

  // Matching result to create response
	var response userResponse
	response.ID = user.ID
	response.Name = user.Name
	response.Email = user.Email
	response.Age = user.Age

  // Creating http response
	context.JSON(http.StatusCreated, response)
}

// Update user data
func UpdateUser(context *gin.Context) {
	var data userRequest
  
  // Defining request parameter to get user id
	reqParamId := context.Param("idUser")
	idUser := cast.ToUint(reqParamId)

  // Binding request body json to request body struct
	if err := context.BindJSON(&data); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

  // Initiate models user
	user := models.User{}

  // Querying find user data by user id from request parameter
	userById := db.Where("id = ?", idUser).First(&user)
	if userById.Error != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "User not found"})
		return
	}

  // Matching user request with user models
	user.Name = data.Name
	user.Email = data.Email
	user.Age = data.Age

  // Update new user data
	result := db.Save(&user)
	if result.Error != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Something went wrong"})
		return
	}
  
  // Matching result to user response struct
	var response userResponse
	response.ID = user.ID
	response.Name = user.Name
	response.Email = user.Email
	response.Age = user.Age

  // Creating http response
	context.JSON(http.StatusCreated, response)
}

// Delete user data function
func DeleteUser(context *gin.Context) {
	// Initiate user models
	  user := models.User{}
	// Getting request parameter id
	  reqParamId := context.Param("idUser")
	  idUser := cast.ToUint(reqParamId)
  
	// Querying delete user by id
	  delete := db.Where("id = ?", idUser).Delete(&user)
	  fmt.Println(delete)
  
	// Creating http response
	  context.JSON(http.StatusOK, gin.H{
		  "status":  "200",
		  "message": "Success",
		  "data":    idUser,
	  })
  
  }