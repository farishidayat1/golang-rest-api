package models

import (
	"gorm.io/gorm"
)

// Defines user table for database communications
type User struct {
	gorm.Model
	Name string
	Email string
	Age int
}