package routes

import (
	"github.com/gin-gonic/gin"
	"rest-api/src/controllers"
)

// Routes function to serve endpoints
func Routes() {
	route := gin.Default()

	route.POST("/user", controllers.CreateUser)
	route.GET("/user", controllers.GetAllUsers)
	route.PUT("/user/:idUser", controllers.UpdateUser)
	route.DELETE("/user/:idUser", controllers.DeleteUser)

  // Run route whenever triggered
	route.Run()
}