package main

import (
	"rest-api/src/config"
	"rest-api/src/routes"
	"gorm.io/gorm"
)

var (
	db *gorm.DB = config.ConnectDB()
)

func main() {
	defer config.DisconnectDB(db)
	
	//run all routes
	routes.Routes()
}